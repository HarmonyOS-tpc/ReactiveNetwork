/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.pwittchen.reactivenetwork.library.rx2.model;

import ohos.net.ConnectionProperties;
import ohos.net.NetCapabilities;
import ohos.net.NetHandle;

/**
 * NetworkState data object
 */
public class NetworkState {
    private boolean isConnected = false;
    private NetHandle netHandle = null;
    private NetCapabilities networkCapabilities = null;
    private ConnectionProperties linkProperties = null;

    /**
     * isConnected
     * @return boolean
     */
    public boolean isConnected() {
        return isConnected;
    }

    /**
     * setConnected
     * @param connected boolean
     */
    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    /**
     * getNetHandle
     * @return NetHandle
     */
    public NetHandle getNetHandle() {
        return netHandle;
    }

    /**
     * setNetHandle
     * @param netHandle NetHandle
     */
    public void setNetHandle(NetHandle netHandle) {
        this.netHandle = netHandle;
    }

    /**
     * getNetworkCapabilities
     * @return NetCapabilities
     */
    public NetCapabilities getNetworkCapabilities() {
        return networkCapabilities;
    }

    /**
     * setNetworkCapabilities
     * @param networkCapabilities NetCapabilities
     */
    public void setNetworkCapabilities(NetCapabilities networkCapabilities) {
        this.networkCapabilities = networkCapabilities;
    }

    /**
     * getLinkProperties
     * @return ConnectionProperties
     */
    public ConnectionProperties getLinkProperties() {
        return linkProperties;
    }

    /**
     * setLinkProperties
     * @param linkProperties ConnectionProperties
     */
    public void setLinkProperties(ConnectionProperties linkProperties) {
        this.linkProperties = linkProperties;
    }
}
