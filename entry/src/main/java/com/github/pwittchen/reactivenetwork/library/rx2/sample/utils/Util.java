/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.pwittchen.reactivenetwork.library.rx2.sample.utils;

/**
 * Util class to provide static methods
 */
public class Util {
    private Util() {
        /* Do nothing */
    }

    /**
     * formatJsonFromString
     * @param value String
     * @return String
     */
    public static String formatJsonFromString(String value) {
        StringBuilder json = new StringBuilder();
        String appendableString = "";

        for (int index = 0; index < value.length(); index++) {
            char letter = value.charAt(index);
            switch (letter) {
                case '{':
                case '[':
                    json.append(System.lineSeparator()).append(appendableString).append(letter).append(System.lineSeparator());
                    appendableString = appendableString + "\t";
                    json.append(appendableString);
                    break;
                case '}':
                case ']':
                    appendableString = appendableString.replaceFirst("\t", "");
                    json.append(System.lineSeparator()).append(appendableString).append(letter);
                    break;
                case ',':
                    json.append(letter).append(System.lineSeparator()).append(appendableString);
                    break;

                default:
                    json.append(letter);
                    break;
            }
        }

        return json.toString();
    }
}
