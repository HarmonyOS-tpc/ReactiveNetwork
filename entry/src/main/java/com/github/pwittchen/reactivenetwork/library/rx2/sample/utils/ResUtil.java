/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.pwittchen.reactivenetwork.library.rx2.sample.utils;

import ohos.agp.components.Component;

/**
 * ResUtil class to provide resource and UI related utility methods in OHOS.
 */
public class ResUtil {
    private ResUtil() {
        /* Do nothing */
    }

    /**
     * find view by id
     *
     * @param view rootView
     * @param id res id
     * @param <T> type
     * @return view
     */
    public static <T extends Component> T findViewById(Component view, int id) {
        if (view == null) {
            return null;
        }
        return (T) view.findComponentById(id);
    }
}
